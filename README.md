# Automação de Testes Mobile
Projeto de Suporte aos Alunos do curso de automação de testes mobile do professor Tiago Samuel Rodrigues Teixeira. Com esse projeto é possível criar testes para aplicativos Android e IOS com Appium, Cucumber e JUnit.


> Esse projeto foi construído a título de aprendizagem, a maior parte dos conteúdos da página principal são estáticos e o sistema não possui algumas funções implementadas. Outrossim, bugs podem ser identificados devido a natureza educacional do projeto. 

##### Estrutura do Projeto
 
| Pasta | Descrição |
| ------ | ------ |
| android-app | App android com os fontes para os alunos poderem alterar ou ajustar |
| curso-automacao-mobile\first-mobile-test-project | Projeto para testes mobile simples com JUnit. |
| curso-automacao-mobile\test-project-bdd-junit5-mobile | Projeto para testes mobile com Cucumber. |

## Links úteis
| Arquivo | Link |
| ------ | ------ |
| VDI (Virtual Box Image) do ambiente de treinamento - SEM CONFIGURAÇÃO | https://drive.google.com/file/d/1WzFSao4JyfMtel7rkk3Wz8PaCOANlTam/view?usp=sharing | 
| VDI (Virtual Box Image) do ambiente de treinamento - COM APPIUM E ANDROID STUDIO | https://drive.google.com/file/d/1f_QIOhyvBNGgKBgwNJ9dgmZQcwB7d3i6/view?usp=sharing | 
| Canal no Youtube (Playlist com os vídeos de treinamento) | https://www.youtube.com/playlist?list=PLruAZvtLCirNkQEa8HbY1qVBs7-89Jcv8 |
| APK do Aplicativo para Android | https://gitlab.com/hiperplano/automacao-testes-mobile-projects/android-app/-/blob/main/app/build/outputs/apk/release/app-release-unsigned.apk |
| IPA do Aplicativo para IOS | TBD |
| URL com a aplicação rodando em modo demo | https://curso-automacao-web-app.herokuapp.com/ |

## Instruções de Instalação
Para poder rodar o ambiente de treinamento é necessário ter o VirtualBox instalado e uma máquina com 16GB de RAM e 250GB SSD (com pelo menos 100GB livre). Para implantar a VDI no VirtualBox basta procurar vídeos de instrução no Youtube. 
Recomendação é utilizar na VM: 

> No mínimo 8GB de RAM;
> 50GB de disco livre;
> 4 núcleos do processador dedicados.

## License
Somente para finalidade educacional, é proibida a distribuição ou comercialização. Em caso de uso deve-se manter o reconhecimento ao autor do projeto. Sua utilização só é permitida após o consentimento expresso do autor. 

For educational purposes only. It's forbidden for commercial or distribution proposes. In case of reuse shall keep the reference of the author of the project. Your usage is only allowed after the expressed formal authorization from author. 
