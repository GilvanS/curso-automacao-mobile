package org.curso.automacao.mobile.bdd.actions;

import org.curso.automacao.mobile.bdd.pages.AutenticacaoPage;
import org.curso.automacao.mobile.bdd.pages.HomePage;
import org.curso.automacao.mobile.bdd.pages.MasterPageFactory;

public class HomeActions {

    public static HomePage homePage(){
        return MasterPageFactory.getPage(HomePage.class);
    }

    public static void validatePage(){
        homePage().validatePage();
        homePage().delay(5000);
    }

    public static void clickMenuMain(){
        homePage().getBtnMenuMain().click();
    }

    public static void clickMenuUsers(){
        homePage().validateMenuMain();
        homePage().getBtnMenuUsers().click();
    }

    public static void clickMenuUsers(boolean clickMenuMain){

        if(clickMenuMain)
            clickMenuMain();

        clickMenuUsers();
    }

}
