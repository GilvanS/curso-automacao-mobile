package org.curso.automacao.mobile.bdd.actions;

import org.curso.automacao.mobile.bdd.Hooks;
import org.curso.automacao.mobile.bdd.pages.AutenticacaoPage;
import org.curso.automacao.mobile.bdd.pages.MasterPageFactory;

public class AutenticacaoActions {

    public static AutenticacaoPage autenticacaoPage(){
        return MasterPageFactory.getPage(AutenticacaoPage.class);
    }

    public static void validatePage(){
        autenticacaoPage().validatePage();
    }

    public static void fillPage(String usuario, String senha){
        AutenticacaoPage autenticacaoPage = autenticacaoPage();
        autenticacaoPage.getTxtUser().sendKeys(usuario);
        autenticacaoPage.getTxtPassword().sendKeys(senha);
    }

    public static void submit(){
        autenticacaoPage().getBtnLogin().click();
    }

}
