package org.curso.automacao.mobile.bdd.steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import lombok.extern.slf4j.Slf4j;
import org.curso.automacao.mobile.bdd.actions.AutenticacaoActions;
import org.curso.automacao.mobile.bdd.actions.HomeActions;

@Slf4j
public class AutenticacaoSteps {

    @Dado("que eu esteja na página de autenticação")
    public void que_eu_esteja_na_página_de_autenticação() {

        log.info("Validar a página de autenticação.");
        AutenticacaoActions.validatePage();

    }

    @Dado("faça o preenchimento dos dados válidos de autenticação com usuário {string} e senha {string}")
    public void faça_o_preenchimento_dos_dados_válidos_de_autenticação_com_usuário_e_senha(String usuario, String senha) {

        log.info("Realizar o preenchimento dos dados de usuário [" + usuario + "] e senha [" + senha + "]");
        AutenticacaoActions.fillPage(usuario, senha);


    }

    @Quando("submeter os dados de autenticação")
    public void submeter_os_dados_de_autenticação() {

        log.info("Submeter os dados");
        AutenticacaoActions.submit();

    }

    @Então("a página inicial será exibida")
    public void a_página_inicial_será_exibida() {

        log.info("Validar a página principal");
        HomeActions.validatePage();

    }

}
